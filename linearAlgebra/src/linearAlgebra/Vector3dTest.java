package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest {
	
	
	@Test
void testGetX() {
		Vector3d a =new Vector3d(5.0,8.0,11.0);
		assertEquals(5.0,a.getX());
	}
	
	@Test
	void testGetY() {
			Vector3d a =new Vector3d(5.0,8.0,11.0);
			assertEquals(8.0,a.getY());
		}
	@Test
	void testGetZ() {
			Vector3d a =new Vector3d(5.0,8.0,11.0);
			assertEquals(11.0,a.getZ());
		}
	
	@Test
	void testMagnitude1() {
		Vector3d a=new Vector3d(2.8,3.4,4.6);
		assertEquals(6.3,a.magnitude(),0.1);
	}
	
	@Test
	void testMagnitude2() {
		Vector3d a=new Vector3d(-3.0,-2.0,-4.0);
		assertEquals(5.3851,a.magnitude(),0.0001);
	}
	
	@Test
	void testMagnitude3() {
	Vector3d a =new Vector3d(0.0,4.0,3.0);
	assertEquals(5.0,a.magnitude());

}

    @Test
    void testDotProduct1() {
    	Vector3d a=new Vector3d(2.9,3.4,4.6);
    	Vector3d b=new Vector3d(2.0,4.0,3.0);
    	assertEquals(33.2,a.dotProduct(b),0.01);
    }

    @Test
    void testDotProduct2() {
    	Vector3d a=new Vector3d(2.0,3.0,4.0);
    	Vector3d b=new Vector3d(0.0,0.0,0.0);
    	assertEquals(0.0,a.dotProduct(b));
    }
    
    @Test
    void testDotProduct3() {
    	Vector3d a=new Vector3d(-3.0,-2.0,-4.0);
    	Vector3d b=new Vector3d(6.0,7.0,10.0);
    	assertEquals(-72.0,a.dotProduct(b));
    }
    
    @Test
    void testAdd1() {
    	Vector3d a=new Vector3d(-3.0,-2.0,-4.0);
    	Vector3d b=new Vector3d(6.0,7.0,10.0);
    	
    	assertEquals(3.0,a.add(b).getX());
    	assertEquals(5.0,a.add(b).getY());
    	assertEquals(6.0,a.add(b).getZ());
    }
    
    @Test
    void testAdd2() {
    	Vector3d a=new Vector3d(0.0,0.0,0.0);
    	Vector3d b=new Vector3d(6.0,7.0,10.0);
    	
    	assertEquals(6.0,a.add(b).getX());
    	assertEquals(7.0,a.add(b).getY());
    	assertEquals(10.0,a.add(b).getZ());
    }
    
    
}