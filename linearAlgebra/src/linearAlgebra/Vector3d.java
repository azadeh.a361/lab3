package linearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
public Vector3d(double x, double y ,double z) {
this.x=x;
this.y=y;
this.z=z;}

public double getX(){
	double x = this.x;
	return x;
}
public double getY() {
	double y =this.y;
	return y;
}
public double getZ() {
	double z=this.z;
	return z;
}
public double magnitude() {
	double magnitude= Math.sqrt(Math.pow(this.getX(),2)+Math.pow(this.getY(),2)+Math.pow(this.getZ(), 2) );
return magnitude;
}

public double dotProduct(Vector3d secondV) {
	double dotProduct=this.getX()*secondV.getX()+this.getY()*secondV.getY()+this.getZ()*secondV.getZ();
	return dotProduct;
}

public Vector3d add(Vector3d secondV) {
	double x =this.getX()+secondV.getX();
	double y =this.getY()+secondV.getY();
	double z =this.getZ()+secondV.getZ();

	Vector3d addUp=new Vector3d(x,y,z);
	return addUp;
	}
	
}
